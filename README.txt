Alright, so when you run the command
"wget -w 2 -m http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en",
you basically download a ton of HTML files with links to compressed text
files. We need to download those compressed files, decompress them, and then
concatenate all of the files into one that we can use to train an RNN. That'll
be a hard one
