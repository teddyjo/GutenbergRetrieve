def extractLine(line, destFile):
    resource = ""
    urlAfterSplit = line.split("<a href=\"")
    if(len(urlAfterSplit) > 1):
        pureURL = urlAfterSplit[1].split("\"")[0]
        if(pureURL[:4] == "http"):
            destFile.write(pureURL + "\n")

with open("wgetData.txt", "r") as f:
    w = open("dataURLs.txt", "w")
    for line in f:
        extractLine(line, w)
