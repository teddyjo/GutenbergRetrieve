#!/bin/bash

echo "How much data would you like? Your options are:"
echo "  (-s) Only a little data (12MB). Thanks!"
echo "  (-m) A decent bit (60GB), I've got time"
echo "  (-l) A lot (1TB). I'm probably gonna get kicked out of Starbucks for this."
echo "  (-f) A fuckton (1 FT). (WARNING: FOR GOOGLE EMPLOYEES ONLY)"

read response

echo "Begin downloading data:"

# Just fixed a nasty bug!
# Turns out, we need the URL in quotes. Otherwise, wget will interpret the "&"
# to mean that it should run in the background. Thus, everything else will fail.

if [ "$response" == "-s" ]; then
  wget -O wgetData.txt -w 2 -Q5k -m "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"
elif [ "$response" == "-m" ]; then
  wget -O wgetData.txt -w 2 -Q25m -m "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"
elif [ "$response" == "-l" ]; then
  wget -O wgetData.txt -w 2 -Q500m -m "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"
elif [ "$response" == "-f" ]; then
  wget -O wgetData.txt -w 2 -m "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"
fi



echo "Done downloading html links!"

# "wget http://aleph.gutenberg.org/etext00/balen10.zip" Works to download data

echo "Extracting links from html:"
python3 extractURLs.py
echo "Done extracting links!"

mkdir downloadedData
mkdir uncompressedData
cd downloadedData

while read p; do
  echo "Downloading:" $p
  wget $p
done <../dataURLs.txt

cd ../uncompressedData

for filename in ../downloadedData/*.zip; do
  unzip $filename
done

for filename in *.txt; do
  cat $filename >> ../finalData.txt
done
